# Project Title  
EasyLeave  
## Getting Started  
These instructions will get you a copy of the project up and running on your local machine 
### Prerequisites 
* Java 8+ installed
*  Latest of Maven installed
* Download/clone the project from BitBucket repository
* Download the latest version of chrome driver from https://chromedriver.chromium.org/downloads
* Download selenium standalone server (Selenium Grid 3) from https://www.selenium.dev/downloads
### Project structure
```
configuration
    +-- config.properties
src/test
    |
    \-- java
        |
        \-- pages
            |
            +-- HomePage
            +-- LeavesPage
        \-- tests
            |
            ++-- LeavesTests
        +-- TestSuite    
    \-- resources
        |
        ++-- TestCases.xls 
pom.xml        
README.md        
```
### Running selenium standalone serve in grid mode
* java -jar selenium-server-standalone-<latest_version>.jar -role hub
* java -Dwebdriver.chrome.driver="<path_to_chromedriver.exe>" -jar selenium-server-standalone-<latest_version>.jar -role node -hub http://localhost:4444/grid/register -port 5555 -browser browserName=chrome
## Running the tests  
* mvn clean test -P QA
