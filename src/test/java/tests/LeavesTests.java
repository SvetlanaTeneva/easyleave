package tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import pages.HomePage;
import pages.LeavesPage;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

public class LeavesTests {

    private WebDriver driver;
    private HomePage homePage;
    private LeavesPage leavesPage;
    private Properties properties = new Properties();
    private static String configPath = "/configuration/config.properties";


    @Test
//    Test case ID#1
    public void testCancelLeaveCreation() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.clickCancelButton();
        String noLeavesMessage = leavesPage.noLeaveAvailable();
        Assert.assertEquals("There is a leave!", LeavesPage.NO_LEAVES_AVAILABLE, noLeavesMessage);
    }

    @Test
//    Test case ID#2
    public void testNewLeaveCreationSuccessMessage() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.clickConfirmButton();
        String actMessage = leavesPage.getSuccessMessage();
        Assert.assertEquals("Message is different", LeavesPage.ADDED_LEAVE_MESSAGE, actMessage);
    }

    @Test
//    Test case ID#3
    public void testFromDateOnLeaveCreation() throws Exception {
        String date = generateFutureDate(3);

        leavesPage.clickAddButton();
        String dateCreated = leavesPage.convertDateSetInLeaveCreation("from", date);
        leavesPage.clickConfirmButton();
        String dateLeave = leavesPage.getLeaveFromDate();
        Assert.assertEquals("Leave is not created correctly", dateLeave, dateCreated);
    }

    @Test
//    Test case ID#4
    public void testToDateOnLeaveCreation() throws Exception {
        String date = generateFutureDate(2);

        leavesPage.clickAddButton();
        String dateCreated = leavesPage.convertDateSetInLeaveCreation("to", date);
        leavesPage.clickConfirmButton();
        String dateLeave = leavesPage.getLeaveToDate();
        Assert.assertEquals("Leave is not created correctly", dateLeave, dateCreated);
    }

    @Test
//    Test case ID#5
    public void testNoFromDate() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.deleteDate("From");
        String actErrorMessage = leavesPage.getErrorMessage();
        Assert.assertEquals("Error message is different!", LeavesPage.ALERT_MESSAGE, actErrorMessage);
    }

    @Test
//    Test case ID#6
    public void testNoToDate() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.deleteDate("To");
        String actErrorMessage = leavesPage.getErrorMessage();
        Assert.assertEquals("Error message is different!", LeavesPage.ALERT_MESSAGE, actErrorMessage);
    }

    @Test
//    Test case ID#7
    public void testInvalidFromDate() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.setDate("from", "13/11/2020");
        String actErrorMessage = leavesPage.getErrorMessage();
        Assert.assertEquals("Error message is different!", LeavesPage.ALERT_MESSAGE, actErrorMessage);
    }

    @Test
//    Test case ID#8
    public void testInvalidToDate() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.setDate("To", "13/11/2020");
        String actErrorMessage = leavesPage.getErrorMessage();
        Assert.assertEquals("Error message is different!", LeavesPage.ALERT_MESSAGE, actErrorMessage);
    }

    @Test
//    Test case ID#9
    public void testStatusDraft() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.clickConfirmButton();
        leavesPage.waitForDraftStatus();
        String actStatus = leavesPage.getLeaveStatus();
        Assert.assertEquals("Status is different", LeavesPage.DRAFT_STATUS, actStatus);
    }

    @Test
//    Test case ID#10
    public void testEditLeaveSuccessMessage() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.clickConfirmButton();
        leavesPage.clickCloseMessageButton();

        leavesPage.clickEditButton();
        leavesPage.clickConfirmButton();
        String actMessage = leavesPage.getSuccessMessage();
        Assert.assertEquals("Message is different", LeavesPage.UPDATED_LEAVE_MESSAGE, actMessage);
    }

    @Test
//    Test case ID#11
    public void testRequestLeaveSuccessMessage() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.clickConfirmButton();
        leavesPage.clickCloseMessageButton();
        leavesPage.clickRequestButton();
        String actMessage = leavesPage.getSuccessMessage();
        Assert.assertEquals("Message is different", LeavesPage.REQUESTED_LEAVE_MESSAGE, actMessage);
    }

    @Test
//    Test case ID#12
    public void testRequestLeaveStatus() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.clickConfirmButton();
        leavesPage.clickRequestButton();
        leavesPage.waitForAcceptedStatus();
        String actStatus = leavesPage.getLeaveStatus();
        Assert.assertEquals("Status is different", LeavesPage.ACCEPTED_STATUS, actStatus);
    }

    @Test
//    Test case ID#13
    public void testExportLeave() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.clickConfirmButton();
        leavesPage.clickRequestButton();
        leavesPage.waitForAcceptedStatus();

        leavesPage.clickExportButton();
        boolean isShown = leavesPage.checkExportWindow();
        Assert.assertTrue(isShown);
    }

    @Test
//    Test case ID#14
    public void testExportCanceled() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.clickConfirmButton();
        leavesPage.clickRequestButton();
        leavesPage.waitForAcceptedStatus();

        leavesPage.clickExportButton();
        leavesPage.clickExportCancelButton();
        boolean isShown = leavesPage.waitForInvisibilityOfExportWindow();
        Assert.assertTrue(isShown);
    }

    @Test
//    Test case ID#15
    public void testDeleteSuccessMessage() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.clickConfirmButton();
        leavesPage.clickCloseMessageButton();

        leavesPage.clickDeleteButton();
        leavesPage.clickConfirmButton();
        String actMessage = leavesPage.getSuccessMessage();
        Assert.assertEquals("Message is different", LeavesPage.DELETED_LEAVE_MESSAGE, actMessage);
    }

    @Test
//    Test case ID16
    public void testDeleteButtonDeletesLeave() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.clickConfirmButton();

        leavesPage.clickDeleteButton();
        leavesPage.clickConfirmButton();
        String noLeavesMessage = leavesPage.noLeaveAvailable();
        Assert.assertEquals("There is a leave!", LeavesPage.NO_LEAVES_AVAILABLE, noLeavesMessage);
    }

    @Test
//    Test case ID#17
    public void testCreatedLeaveShownAsEventInCalendar() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.clickConfirmButton();

        int events = leavesPage.getEventsInCalendar();
        Assert.assertTrue(events > 0);
    }

    @Test
//    Test case ID#18
    public void testDraftLeaveShownInBlueInCalendar() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.clickConfirmButton();

        String actColor = leavesPage.getColorOfEventInCalendar();
        Assert.assertEquals("Color is different!", LeavesPage.COLOR_BLUE, actColor);
    }

    @Test
//    Test case ID#19
    public void testRequestedLeaveShownInGreenInCalendar() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.clickConfirmButton();
        leavesPage.clickRequestButton();

        String actColor = leavesPage.getColorOfEventInCalendar();
        Assert.assertEquals("Color is different!", LeavesPage.COLOR_GREEN, actColor);
    }

    @Test
//    Test case ID#20
    public void testShowNameInCalendar() throws Exception {
        leavesPage.clickAddButton();
        leavesPage.clickConfirmButton();

        leavesPage.clickTodayOnCalendar();
        String actName = leavesPage.getName();
        Assert.assertEquals("Name is different!", LeavesPage.MY_NAME, actName);
    }

    @Test
//    Test case ID#21
    public void testGermanVersion() throws Exception {
        leavesPage.selectGermanLanguage();
        String dashboardPageMessage = leavesPage.getPersonalLeaveText();
        Assert.assertEquals("Language is different", LeavesPage.PERSONAL_LEAVES_TEXT, dashboardPageMessage);
    }

    @Test
//    Test case ID#22
    public void testBulgarianVersion() throws Exception {
        leavesPage.selectBulgarianLanguage();
        String dashboardPageMessage = leavesPage.getPersonalLeaveText();
        Assert.assertEquals("Language is different", LeavesPage.PERSONAL_LEAVES_BGTEXT, dashboardPageMessage);
    }

    @Before
    public void before() throws Exception {
        initDriver();
        logIntoEasyLeave();
    }

    @After
    public void tearDown() throws Exception {
        try {
            leavesPage.closeLeavePopup();
            leavesPage.deleteLeaves();
            leavesPage.logout();
        } finally {
            driver.quit();
        }
    }

    private void initDriver() throws IOException {
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        capability.setBrowserName("chrome");
        driver = new RemoteWebDriver(new URL(getConfiguration().getProperty("hubURL")), capability);
        driver.get(getConfiguration().getProperty("home"));
        driver.manage().window().maximize();
    }

    private Properties getConfiguration() throws IOException {
        final InputStream inputStream = new FileInputStream(System.getProperty("user.dir") + configPath);
        properties.load(inputStream);
        return properties;
    }

    private void logIntoEasyLeave() throws Exception {
        homePage = new HomePage(driver);
        leavesPage = new LeavesPage(driver);

        homePage.login(getConfiguration().getProperty("username"), getConfiguration().getProperty("password"));
        String dashboardPageMessage = leavesPage.getPersonalLeaveText();
        Assert.assertEquals("User is not logged in", LeavesPage.PERSONAL_LEAVES_TEXT, dashboardPageMessage);
    }

    private String generateFutureDate(int days) {
        return LocalDate.now().plusDays(days).format(DateTimeFormatter.ofPattern("MM/d/yyyy"));
    }
}
