import org.junit.runners.Suite;
import org.junit.runner.RunWith;
import tests.LeavesTests;

@RunWith(Suite.class)
@Suite.SuiteClasses({LeavesTests.class})
public class TestSuite {
}
