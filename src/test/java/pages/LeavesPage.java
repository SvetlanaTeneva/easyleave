package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

import static java.lang.Integer.parseInt;

public class LeavesPage {
    private static final String PERSONAL_LEAVES_ELEMENT = ".ng-star-inserted > h2";
    private static final String ADD_BUTTON = "app-leaves button";
    private static final String ACTIONS_BUTTON = "mat-dialog-actions button";
    private static final String NO_LEAVES_ELEMENT = "//tr[@class='empty fullwidth ng-star-inserted']/td";
    private static final String LEAVE_STATUS = "table tr:nth-child(2) td:nth-child(5)";
    private static final String ACCEPTED_STATUS_ELEMENT = ".accepted";
    private static final String DRAFT_STATUS_ELEMENT = ".past";
    private static final String FROM_DATE = "fromDate";
    private static final String TO_DATE = "toDate";
    private static final String FORM_ELEMENT = "form";
    private static final String LEAVE_FROM_DATE = "//tr[@class='ng-star-inserted']/td[1]";
    private static final String LEAVE_TO_DATE = "//tr[@class='ng-star-inserted']/td[2]";
    private static final String EXPORT_WINDOW = "mat-dialog-content";
    private static final String EDIT_BUTTON = "table tr:nth-child(2) button";
    private static final String REQUEST_BUTTON = "table tr:nth-child(2) button:nth-child(2)";
    private static final String DELETE_BUTTON = "table tr:nth-child(2) button:nth-child(3)";
    private static final String DEL_BUTTON = ".ng-star-inserted td:nth-child(6) button:last-child";
    private static final String EXPORT_BUTTON = "table tr:nth-child(2) button";
    private static final String EXPORT_CANCEL_BUTTON = "button[aria-label='Close dialog']";
    private static final String MESSAGE_CLOSE_BUTTON = ".close";
    private static final String CALENDAR_EVENTS = ".cal-today.cal-in-month.cal-has-events.ng-star-inserted .cal-day-badge.ng-star-inserted";
    private static final String CALENDAR_TODAY = ".cal-today.cal-in-month.cal-has-events.ng-star-inserted";
    private static final String CALENDAR_COLORED_EVENTS = ".cal-today.cal-in-month.cal-has-events.ng-star-inserted .cal-event.ng-star-inserted";
    private static final String NAMES_LIST = ".cal-open-day-events .cal-event-title";
    private static final String LANGUAGE_DROPDOWN = "language";
    private static final String LANGUAGE_OPTIONS = ".mat-select-panel .mat-option-text";
    private static final String ACCOUNT_MENU = "accountMenu";
    private static final String LOGOUT_LINK = ".dropdown-menu.show .dropdown-item:nth-child(6)";
    private static final String CSS = "css";
    private static final String XPATH = "xpath";

    public static final String PERSONAL_LEAVES_TEXT = "Personal leaves";
    public static final String PERSONAL_LEAVES_BGTEXT = "Личен отпуск";
    public static final String NO_LEAVES_AVAILABLE = "No planned leaves";
    public static final String DRAFT_STATUS = "Draft";
    public static final String ACCEPTED_STATUS = "Accepted";
    public static final String ALERT_MESSAGE = "Not enough days of leave";
    public static final String REQUESTED_LEAVE_MESSAGE = "leave successfully requested.";
    public static final String ADDED_LEAVE_MESSAGE = "leave successfully added.";
    public static final String UPDATED_LEAVE_MESSAGE = "leave successfully updated.";
    public static final String DELETED_LEAVE_MESSAGE = "leave successfully deleted.";
    public static final String COLOR_BLUE = "blue";
    public static final String COLOR_GREEN = "green";
    public static final String MY_NAME = "Svetlana Teneva";


    private WebDriver driver;

    private WebElement waitForVisibility(String elLocator, String selType) throws Exception {
        if (selType.equals(CSS))
            return new WebDriverWait(driver, 10)
                    .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(elLocator)));
        else if (selType.equals(XPATH))
            return new WebDriverWait(driver, 10)
                    .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elLocator)));
        else throw new Exception("Unknown type!");
    }

    private void clickLanguageDropdown() {
        driver.findElement(By.className(LANGUAGE_DROPDOWN)).click();
    }

    public LeavesPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getPersonalLeaveText() throws Exception {
        return waitForVisibility(PERSONAL_LEAVES_ELEMENT, CSS).getText();
    }

    public void waitForAcceptedStatus() throws Exception {
        waitForVisibility(ACCEPTED_STATUS_ELEMENT, CSS);
    }

    public void waitForDraftStatus() throws Exception {
        waitForVisibility(DRAFT_STATUS_ELEMENT, CSS);
    }

    public void clickAddButton() {
        driver.findElements(By.cssSelector(ADD_BUTTON)).get(0).click();
    }

    public void clickCancelButton() {
        driver.findElements(By.cssSelector(ACTIONS_BUTTON)).get(1).click();
    }

    public String noLeaveAvailable() throws Exception {
        waitForVisibility(NO_LEAVES_ELEMENT, XPATH);
        return driver.findElement(By.xpath(NO_LEAVES_ELEMENT)).getText();
    }

    public void clickConfirmButton() throws Exception {
        waitForVisibility(ACTIONS_BUTTON, CSS);
        driver.findElements(By.cssSelector(ACTIONS_BUTTON)).get(0).click();
    }

    public String getLeaveStatus() {
        return driver.findElement(By.cssSelector(LEAVE_STATUS)).getText();
    }

    public void deleteDate(String field) throws Exception {
        WebElement el;
        if (field.toLowerCase().equals("from")) {
            el = driver.findElement(By.id(FROM_DATE));
        } else if (field.toLowerCase().equals("to")) {
            el = driver.findElement(By.id(TO_DATE));
        } else
            throw new Exception("Unknown date field!");

        el.click();
        el.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        driver.findElement(By.cssSelector(FORM_ELEMENT)).click();
    }

    public String getErrorMessage() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        return js.executeScript("return document.getElementsByClassName('alert alert-danger')[0].innerHTML;").toString().trim();
    }

    public String getSuccessMessage() throws Exception {
        waitForVisibility(MESSAGE_CLOSE_BUTTON, CSS);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        return js.executeScript("return document.getElementsByClassName('alert alert-success')[0].innerText;").toString().replaceAll("\\n|\\×", "");
    }

    public void clickCloseMessageButton() throws Exception {
        waitForVisibility(MESSAGE_CLOSE_BUTTON, CSS);
        driver.findElement(By.cssSelector(MESSAGE_CLOSE_BUTTON)).click();
    }

    public void clickRequestButton() throws Exception {
        waitForVisibility(REQUEST_BUTTON, CSS);
        driver.findElement(By.cssSelector(REQUEST_BUTTON)).click();
    }

    public String setDate(String field, String date) throws Exception {
        WebElement el;
        if (field.toLowerCase().equals("from"))
            el = driver.findElement(By.id(FROM_DATE));
        else if (field.toLowerCase().equals("to"))
            el = driver.findElement(By.id(TO_DATE));
        else throw new Exception("Unknown field!");

        el.click();
        el.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        el.sendKeys(date);
        driver.findElement(By.cssSelector(FORM_ELEMENT)).click();
        return date;
    }

    public String convertDateSetInLeaveCreation(String field, String dateStr) throws Exception {
        String dateSet = setDate(field, dateStr);
        return LocalDate.parse(dateSet, DateTimeFormatter.ofPattern("MM/d/yyyy", Locale.US)).
                format(DateTimeFormatter.ofPattern("MMM d, yyyy", Locale.US));
    }

    public String getLeaveFromDate() throws Exception {
        waitForVisibility(LEAVE_FROM_DATE, XPATH);
        return driver.findElement(By.xpath(LEAVE_FROM_DATE)).getText();
    }

    public String getLeaveToDate() throws Exception {
        waitForVisibility(LEAVE_TO_DATE, XPATH);
        return driver.findElement(By.xpath(LEAVE_TO_DATE)).getText();
    }

    public void clickEditButton() throws Exception {
        waitForVisibility(EDIT_BUTTON, CSS);
        driver.findElement(By.cssSelector(EDIT_BUTTON)).click();
    }

    public void clickExportButton() {
        driver.findElement(By.cssSelector(EXPORT_BUTTON)).click();
    }

    public void clickDeleteButton() throws Exception {
        waitForVisibility(DELETE_BUTTON, CSS);
        driver.findElement(By.cssSelector(DELETE_BUTTON)).click();
    }

    public boolean checkExportWindow() {
        return driver.findElement(By.cssSelector(EXPORT_WINDOW)).isDisplayed();
    }

    public boolean waitForInvisibilityOfExportWindow() {
        return new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(EXPORT_WINDOW)));
    }

    public void clickExportCancelButton() {
        driver.findElement(By.cssSelector(EXPORT_CANCEL_BUTTON)).click();
    }

    public int getEventsInCalendar() throws Exception {
        driver.navigate().refresh();
        waitForVisibility(CALENDAR_EVENTS, CSS);
        String events = driver.findElement(By.cssSelector(CALENDAR_EVENTS)).getText();
        return parseInt(events);
    }

    public String getColorOfEventInCalendar() throws Exception {
        driver.navigate().refresh();
        waitForVisibility(CALENDAR_EVENTS, CSS);
        List<WebElement> events = driver.findElements(By.cssSelector(CALENDAR_COLORED_EVENTS));
        if (!events.isEmpty()) {
            String style = events.get(0).getAttribute("style");
            return style.split("background-color: ")[1].replace(";", "");
        } else
            throw new Exception("No events found!");
    }

    public void clickTodayOnCalendar() throws Exception {
        driver.navigate().refresh();
        waitForVisibility(CALENDAR_TODAY, CSS);
        driver.findElement(By.cssSelector(CALENDAR_TODAY)).click();
    }

    public String getName() throws Exception {
        List<WebElement> names = driver.findElements(By.cssSelector(NAMES_LIST));
        if (!names.isEmpty())
            return names.get(0).getText();
        else throw new Exception("No names found!");
    }

    public void selectGermanLanguage() {
        clickLanguageDropdown();
        driver.findElements(By.cssSelector(LANGUAGE_OPTIONS)).get(1).click();
    }

    public void selectBulgarianLanguage() {
        clickLanguageDropdown();
        driver.findElements(By.cssSelector(LANGUAGE_OPTIONS)).get(2).click();
    }

    public void deleteLeaves() throws Exception {
        List<WebElement> elements = driver.findElements(By.cssSelector(DEL_BUTTON));
        if(elements.size() > 0) {
            for (WebElement e : elements) {
                e.click();
                clickConfirmButton();
            }
        }
        noLeaveAvailable();
    }

    public void closeLeavePopup() {
        List<WebElement> actionsButtons = driver.findElements(By.cssSelector(ACTIONS_BUTTON));
        if (actionsButtons.size() > 1)
            actionsButtons.get(1).click();
    }

    public void logout() {
        driver.findElement(By.id(ACCOUNT_MENU)).click();
        driver.findElement(By.cssSelector(LOGOUT_LINK)).click();
    }
}

