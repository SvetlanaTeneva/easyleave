package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {

    private static final String USERNAME_FIELD = "emailAddress";
    private static final String PASSWORD_FIELD = "password";
    private static final String LOGIN_BUTTON = "logButton";

    private WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void login(String username, String password) {
        driver.findElement(By.name(USERNAME_FIELD)).sendKeys(username);
        driver.findElement(By.name(PASSWORD_FIELD)).sendKeys(password);
        driver.findElement(By.id(LOGIN_BUTTON)).click();
    }
}
